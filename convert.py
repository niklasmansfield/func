def m2s(minutes):
    """Convert the number of minutes to seconds and returns it."""
    return 60 * minutes


def h2m(hours):
    """Convert the number of hours to minutes and returns it."""
    return 60 * hours


def d2s(days):
    """Convert the number of days to seconds and returns it."""
    return 60 * 60 * 24 * days


def d2m(days):
    """Convert the number of days to minutes and returns it."""
    return 60 * 24 * days


def d2h(days):
    """Convert the number of days to hours( and returns it."""
    return 24 * days

