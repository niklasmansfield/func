import convert

days = 5
print(str(days) + " days is: " + str(convert.d2h(days)) + " hours, or " + 
      str(convert.d2m(days)) + " minutes, or " +
      str(convert.d2s(days)) + " seconds.")
