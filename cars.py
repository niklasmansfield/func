
import math

# Koeningsegg Agera R max speed is 440 km/h
agera_r_ms = 440

# Some french bus
freddes_franska_buss_ms = 130

# Some radiuses
r_earth = 6371
r_moon = 1738
r_mars = 3396
r_sun = 695500

def stupid_func(sphere, vehicle, radius, speed=90):
    return 'Ett varv runt ' + sphere + ' ekvator med en ' + vehicle + ' tar: ' + str(round(radius*2*math.pi // speed)) + " timmar och "  + str(round((radius*2*math.pi % speed) / speed*60)) + " minuter."

print(stupid_func('jordens', 'Agera R', r_earth, agera_r_ms))
print(stupid_func('månens', 'Agera R', r_moon, agera_r_ms))
print(stupid_func('mars', 'Agera R', r_mars, agera_r_ms))
print(stupid_func('solens', 'Agera R', r_sun, agera_r_ms))
print('')
print(stupid_func('jordens', 'Freddes buss', r_earth, freddes_franska_buss_ms))
print(stupid_func('månens', 'Freddes buss', r_moon, freddes_franska_buss_ms))
print(stupid_func('mars', 'Freddes buss', r_mars, freddes_franska_buss_ms))
print(stupid_func('solens', 'Freddes buss', r_sun, freddes_franska_buss_ms))


