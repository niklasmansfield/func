
# there are 10 kinds of people in this world. Those who can read binary and those who can't

# Assignment 1 
#-------------
# Write a function that turns a number into binary 
# experiment with the builtin function bin() 
# example num2bin(23) should return '0b10111'

def num2bin(decimal_number):
    return bin(decimal_number)


# Assignment 2 
#-------------
# This function returns True if the number is positive and in the interval 0 - 31
# Change it so that it can take start and stop arguments for any interval
# Make sure that the default value for start == -1 for stop == 31

def verify_range(number, x=0, y=31):
	if number >= x and number <= y:
		return True
	else:
		return False

# Assignment 3
#-------------
# Create a function that takes a number as argument and returns a string of an integer and it's binary number format
# uses verify_range() to make sure that the number is between 0 and 16
# return "Error" if the number is out of range.
# use num2bin() for the convertion

def num2bin_in_str(number):
    if verify_range(number, 0, 32):
        return str(number) + '=' + num2bin(number)
    else:
        return 'Error - number out of range.'


# Assignment 4
#-------------
# Import this module into the interpreter
# Try to write code that prints out the number to binary conversion between 0 and 32 using the while statement
# and the print() function

def nice_format(string, horizontal_space):
    return (horizontal_space - len(string)) * ' ' + string

counter = 0
print('')
print('Number 0 through 31 in decimal and binary form:')
print('')
while verify_range(counter, 0, 31):
    print(nice_format(str(counter), 3) + ' = ' + nice_format(bin(counter),7))
    counter += 1
print('')
